package com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.swagger.config.SwaggerConfig;

@SpringBootApplication
@Import(SwaggerConfig.class)
@ComponentScan(basePackages = {"com.example.service", "com.example.controllers", "com.example.mapper"})
@EntityScan(basePackages = "com.example.entity") 
@EnableJpaRepositories(basePackages = {"com.example.repository"})
public class MainApplication {
	
	 public static void main(String[] args) {
	        
		 SpringApplication.run(MainApplication.class, args);
	    }	
	}


