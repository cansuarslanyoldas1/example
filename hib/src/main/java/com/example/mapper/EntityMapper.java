package com.example.mapper;

import org.springframework.stereotype.Component;

import com.example.dto.DepartmentsDTO;
import com.example.dto.EmployeeDTO;
import com.example.entity.Departments;
import com.example.entity.Employee;

@Component
public class EntityMapper {
    public Employee mapDTOToEmployee(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        employee.setName(employeeDTO.getName());
        employee.setSurname(employeeDTO.getSurname());      
        return employee;
    }

    public EmployeeDTO mapEmployeeToDTO(Employee employee) {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setId(employee.getId());
        dto.setName(employee.getName());
        dto.setSurname(employee.getSurname());        
        return dto;
    }
    
    public DepartmentsDTO mapDepartmentToDTO(Departments department) {
        DepartmentsDTO dto = new DepartmentsDTO();
        dto.setId(department.getId());
        dto.setName(department.getName());      
        return dto;
    }
}





