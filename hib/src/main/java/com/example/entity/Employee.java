package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "EMPLOYEE")

public class Employee {

	@Id
	@Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(hidden = true)
    private Long id;
	@ApiModelProperty(value="Employee Name")
	@Column(name = "name")
    private String name;
	@ApiModelProperty(value="Employee Surname")
	@Column(name = "surname")
    private String surname;
    

    public Employee() {
      
    }
    public Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    
    public Employee(long id, String name, String surname) {
    	this.id = id;
        this.name = name;
        this.surname = surname;
    }
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

    
}