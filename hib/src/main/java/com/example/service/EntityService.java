package com.example.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.DepartmentsDTO;
import com.example.dto.EmployeeDTO;
import com.example.entity.Departments;
import com.example.entity.Employee;
import com.example.mapper.EntityMapper;
import com.example.repository.IDepartmentsRepository;
import com.example.repository.IEmployeeRepository;

@Service
public class EntityService {

	@Autowired
    private IEmployeeRepository employeeRepository;
	
	@Autowired
    private IDepartmentsRepository departmentsRepository;

    @Autowired
    private EntityMapper entityMapper;

    public List<EmployeeDTO> getAllEmployees() {
        List<Employee> employees = employeeRepository.findAll();
        return employees.stream()
            .map(entityMapper::mapEmployeeToDTO)
            .collect(Collectors.toList());
    }
    
    public EmployeeDTO getEmployeeById(Long id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            return entityMapper.mapEmployeeToDTO(employee);
        }
        return null; 
    }
    
    public EmployeeDTO createEmployee(EmployeeDTO employeeDTO) {
        Employee employee = entityMapper.mapDTOToEmployee(employeeDTO);
        Employee createdEmployee = employeeRepository.save(employee);
        EmployeeDTO createdEmployeeDTO = entityMapper.mapEmployeeToDTO(createdEmployee);

        return createdEmployeeDTO;
    }
    
    public List<DepartmentsDTO> getAllDepartments() {
        List<Departments> departments = departmentsRepository.findAll();
        return departments.stream()
            .map(entityMapper::mapDepartmentToDTO)
            .collect(Collectors.toList());
    }
}
