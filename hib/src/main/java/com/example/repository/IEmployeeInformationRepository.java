package com.example.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.entity.EmployeeInformation;


public interface IEmployeeInformationRepository extends JpaRepository<EmployeeInformation, Long>  {
			
	List<EmployeeInformation> findByEmployeeId(Long employeeId);
	
	<S extends EmployeeInformation> S save(S entity);
	
	@Query("SELECT AVG(e.salary) FROM EmployeeInformation e")
    Double calculateAverageSalary();
}
