package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Departments;

public interface IDepartmentsRepository extends JpaRepository<Departments, Long> {
			
	<S extends Departments> S save(S entity);
}
