package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Company;

public interface ICompanyRepository extends JpaRepository<Company, Long> {

	<S extends Company> S save(S entity);
}
