package com.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.EmployeeDTO;
import com.example.service.EntityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping
@Api(tags = "Employee")
public class EmployeeController {
    @Autowired
    private EntityService entityService;
    
    @GetMapping("GetAllEmployees")
    @ApiOperation(value = "Get all employees")
    public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
        List<EmployeeDTO> employees = entityService.getAllEmployees();
        return ResponseEntity.ok(employees);
    }

    @GetMapping("GetEmployeeById/{id}")
    @ApiOperation(value = "Get employee by ID", notes = "This API endpoint allows you to get an employee by their ID.")
    @ApiImplicitParam(name = "id", value = "Employee ID", required = true, dataType = "long", paramType = "path")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable Long id) {
        EmployeeDTO employee = entityService.getEmployeeById(id);
        if (employee != null) {
            return ResponseEntity.ok(employee);
        }
        return ResponseEntity.notFound().build();
    }
    
    @PostMapping("AddEmployee")
    @ApiOperation(value = "Create a new employee", notes = "This API endpoint allows you to create a new employee.")
    @ApiImplicitParam(name = "employeeDTO", value = "Employee data as JSON", required = true, dataType = "EmployeeDTO", paramType = "body")
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        EmployeeDTO createdEmployeeDTO = entityService.createEmployee(employeeDTO);
        
        if (createdEmployeeDTO != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdEmployeeDTO);
        }
        
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    
    
}