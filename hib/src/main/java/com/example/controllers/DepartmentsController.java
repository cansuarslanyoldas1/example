package com.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.DepartmentsDTO;
import com.example.service.EntityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/Departments")
@Api(tags = "Departments")
public class DepartmentsController {
	
	@Autowired
    private EntityService entityService;

	@GetMapping("GetAllDepartments")
    @ApiOperation(value = "Get all departments", notes = "This API endpoint allows you to get all departments.")
    public ResponseEntity<List<DepartmentsDTO>> getAllDepartments() {
        List<DepartmentsDTO> departmentDTOs = entityService.getAllDepartments();
        return ResponseEntity.ok(departmentDTOs);
    }
}
